import { Component, OnInit } from '@angular/core';
import { RedditService } from '../service/reddit.service';
import { Settings } from '../model/settings';
import { SettingsService } from '../service/settings.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  settings: Settings = new Settings();

  posts: Array<any> = [];

  constructor(private redditService: RedditService,
    private settingsService: SettingsService) {}

  async ngOnInit() {
    await this.settingsService.getSettings()
    .then((settings) => this.settings = settings);
  
    this.loadPosts();
  }

  updatePosts() {
    this.loadPosts();
  }

  async loadPosts() {
    await this.redditService.getPosts(this.settings)
      .subscribe((redditPosts) => {
        this.posts = redditPosts.data.children;
      });
  }
}
