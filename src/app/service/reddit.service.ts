import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Settings } from '../model/settings';

@Injectable({
  providedIn: 'root'
})
export class RedditService {
  url: string = 'https://www.reddit.com';

  constructor(private http: HttpClient) { }

  getPosts(settings: Settings): Observable<any> {
    return this.http.get(`${this.url}/r/${settings.subreddit}/top.json?limit=${settings.limit}`);
  }
}
