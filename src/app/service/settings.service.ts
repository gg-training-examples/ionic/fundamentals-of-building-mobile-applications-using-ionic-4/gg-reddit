import { Injectable } from '@angular/core';
import { Settings } from '../model/settings';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor(private storage: Storage) { }

  getSettings(): Promise<Settings> {
    return new Promise<Settings>(resolve => {
      this.storage.get('reddit-settings').then((data) => {
        if (data) {
          resolve(data);
        } else {
          resolve({
              subreddit: 'music',
              limit: 10
            });
        }
      });
    });
  }

  saveSettings(settings: Settings): Promise<Settings> {
    return this.storage.set('reddit-settings', settings);
  }
}
