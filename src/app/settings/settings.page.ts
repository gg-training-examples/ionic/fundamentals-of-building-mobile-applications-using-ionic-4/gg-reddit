import { Component, OnInit } from '@angular/core';
import { Settings } from '../model/settings';
import { SettingsService } from '../service/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  settings: Settings = new Settings();

  constructor(private settingsService: SettingsService) { }

  ngOnInit() {
    this.loadSettings();
  }

  async loadSettings() {
    this.settings = await this.settingsService.getSettings();
  }

  async saveSettings() {
    await this.settingsService.saveSettings(this.settings);
    console.log('save successful');
  }
}
